const pad = (hex)=>{
    return (hex.length === 1 ? "0" + hex : hex);
}

module.exports={
    rgbToHex: (red,green,blue) => {
        const redHex= red.toString(16); // ff
        const greenHex= green.toString(16);
        const blueHex= blue.toString(16);

        return pad(redHex) + pad(greenHex) + pad(blueHex); //ff00ff
    },
    hexToRGB:(red,green,blue)=>{
        const redRGB= parseInt(red, 16);
        const greenRGB=parseInt(green, 16);
        const blueRGB=parseInt(blue, 16);

        return redRGB.toString()+" "+ greenRGB.toString()+" "+  blueRGB.toString();
    }
}