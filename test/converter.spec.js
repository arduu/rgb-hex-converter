// TDD -Test Driven Developement -Unit testing

const expect = require('chai').expect;
const converter = require('../src/converter');

describe("color code converter", () => {
    describe("RGB to HEX conversion", () => {
        it("converts the basic colors", () => {
            const redHex = converter.rgbToHex(255, 0, 0); // ff0000
            const greenHex = converter.rgbToHex(0, 255, 0); // 00ff00
            const blueHex = converter.rgbToHex(0, 0, 255); // 0000ff
        
            expect(redHex).to.equal("ff0000");
            expect(greenHex).to.equal("00ff00");
            expect(blueHex).to.equal("0000ff");
        });
    });

    describe("HEX to RGB conversion", () => {
        it("converts the basic colors", () => {
            const redRGB = converter.hexToRGB('ff','00','00'); // 255,0,0
            const greenRGB = converter.hexToRGB('00','ff','00'); // 0,255,0
            const blueRGB = converter.hexToRGB('00','00','ff'); // 0,0,255
        
            expect(redRGB).to.equal('255 0 0')
            expect(greenRGB).to.equal('0 255 0')
            expect(blueRGB).to.equal('0 0 255')
        });
    });
    
});